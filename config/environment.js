'use strict';


module.exports = function(environment) {

  var ENV = {
    environment: environment || 'production',
    mongolabURI: 'mongodb://userdemo:userpass@ds061641.mongolab.com:61641/apidemo',
    port: 8080,
    ip:'127.0.0.1'
  };

  if (environment === 'development') {
    ENV.environment = 'development';
    ENV.mongolabURI = 'mongodb://userdemo:userpass@ds061641.mongolab.com:61641/apidemo';
  }

  if (environment === 'test') {
    ENV.environment = 'test';
    ENV.mongolabURI = 'mongodb://userdemo:userpass@ds061641.mongolab.com:61641/apidemo';
  }

  if (environment === 'production') {
    ENV.environment = 'production';
    ENV.mongolabURI = 'mongodb://userdemo:userpass@ds061641.mongolab.com:61641/apidemo';
    ENV.port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
    ENV.ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
  }

  return ENV;
};
