'use strict';

/**
 * Module dependencies
 */
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors');

/**
 * Locals
 */
var app = module.exports = express();
var environment = require('./config/environment');
var config = environment(process.env.NODE_ENV);
var mongolabURI = config.mongolabURI;
var port = config.port;
var ip = config.ip;

// parse json requests
app.use( bodyParser.urlencoded({ extended: true }));
app.use( bodyParser.json('application/json') );
app.use(cors());

/**
 * Routes
 */
var wellcome = require('./lib/wellcome');
var noticias = require('./lib/noticias');

app.use(wellcome);
app.use(noticias);

/**
 * Start server if we're not someone else's dependency
 */

console.log('Environment: ' + config.environment);

if( config.environment === 'development'){
  if (!module.parent) {
    // Conectamos con la base de datos y después lanzamos la apliación
    mongoose.connect(mongolabURI, function() {
      app.listen( 3000 , function() {
        console.log( 'API demo ready in port: ' + 3000 );
      });
    });
  }
}


if( config.environment === 'production'){
  mongoose.connect(mongolabURI, function() {
   app.listen(port, ip, function() {
     console.log( 'API demo ready in port: ' + port + ' in ip: ' + ip );
   });
  });
}
