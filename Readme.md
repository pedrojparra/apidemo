# Api demo.

## Metodos HTTP permitidos

|  Metodo  |              Descripcion               |
| -------- | -------------------------------------- |
| `GET`    | Obtener un recurso o lista de recursos |
| `POST`   | Crear un recurso                       |
| `PUT`    | Actualizar un recurso                  |
| `DELETE` | Eliminar un recurso                    |

## Codigos de Respuesta

| Codigo |                         Descripcion                          |
| ------ | ------------------------------------------------------------ |
| `200`  | Success                                                      |
| `201`  | Success - nuevo recurso creado.                              |
| `204`  | Success - no hay contenido para responder                    |
| `400`  | Bad Request - i.e. su solicitud no se pudo evaluar           |
| `401`  | Unauthorized - usuario no esta autenticado para este recurso |
| `404`  | Not Found - recurso no existe                                |
| `422`  | Unprocessable Entity - i.e. errores de validación            |
| `429`  | Limite de uso excedido, intente mas tarde                    |
| `500`  | Error de servidor                                            |
| `503`  | Servicio no disponible                                       |

## Noticias

#### Obtener una noticias

Solicitud [GET] /api/v1/noticias

Respuesta
```json
{
  "noticias":[
    {
      "id":"155",
      "titulo":"Demo de noticia",
      "contenido":"Contenido de noticia",
      "createdAt": "2015-04-15T10:58:35.040Z",
      "updatedAt": "2015-04-15T10:58:35.040Z",
      "category": [ "news", "actualidad", "ocio" ],
      "keywords": [ "contenido", "noticia", "demo" ],
      "slug":"demo-de-noticia"
    },
    {
      "id":"156",
      "titulo":"Demo de noticia 2",
      "contenido":"Contenido de noticia 2",
      "createdAt": "2015-04-15T10:58:35.248Z",
      "updatedAt": "2015-04-15T10:58:35.410Z",
      "category": [ "news", "actualidad", "ocio" ],
      "keywords": [ "cultura" ],
      "slug":"demo-de-noticia-2"
    },
    {
      "id":"155",
      "titulo":"Demo de noticia 3",
      "contenido":"Contenido de noticia 3",
      "createdAt": "2015-04-15T10:58:35.040Z",
      "updatedAt": "2015-04-15T10:58:35.040Z",
      "category": [ "news", "actualidad", "ocio" ],
      "keywords": [ "contenido", "noticia", "demo" ],
      "slug":"demo-de-noticia-3"
    },
    {
      "id":"156",
      "titulo":"Demo de noticia 4",
      "contenido":"Contenido de noticia 4",
      "createdAt": "2015-04-15T10:58:35.248Z",
      "updatedAt": "2015-04-15T10:58:35.410Z",
      "category": [ "news", "actualidad", "ocio" ],
      "keywords": [ "cultura" ],
      "slug":"demo-de-noticia-3"
    }
  ],
  "stat":"ok"
}
```

### Paginacion de resultados

Solicitud [GET] /api/v1/noticias/1/1
noticias/:page/:per_page





### Crear una nueva noticia

Solicitud [POST] /api/v1/noticias
```json
{
  "noticia":{
    "titulo":"Demo de noticia",
    "contenido":"Contenido de noticia",
    "category": [ "news", "actualidad", "ocio" ],
    "keywords": [ "contenido", "noticia", "demo" ]
  }
}
```
Respuesta ok
```json
{
  "noticia":{
    "id":"155",
    "titulo":"Demo de noticia",
    "contenido":"Contenido de noticia",
    "createdAt": "2015-04-15T10:58:35.040Z",
    "updatedAt": "2015-04-15T10:58:35.040Z",
    "category": [ "news", "actualidad", "ocio" ],
    "keywords": [ "contenido", "noticia", "demo" ],
    "slug":"demo-de-noticia"
  },
  "stat":"ok"
}
```
Respuesta fail
```json
{
  "stat": "fail",
  "error": 422,
  "type": "Unprocessable Entity",
  "info": {
    "message": "noticias validation failed",
     "name": "ValidationError",
     "errors": {
       "titulo": [Object]
      }
  }
}

```


#### Obtener una noticia

Solicitud [GET] /api/v1/noticias/155

Respuesta ok
```json
{
  "noticia":{
    "id":"155",
    "titulo":"Demo de noticia",
    "contenido":"Contenido de noticia",
    "createdAt": "2015-04-15T10:58:35.040Z",
    "updatedAt": "2015-04-15T10:58:35.040Z",
    "category": [ "news", "actualidad", "ocio" ],
    "keywords": [ "contenido", "noticia", "demo" ],
    "slug":"demo-de-noticia"
  },
  "stat":"ok"
}
```
### Actualizar una noticia

Solicitud [PUT] /api/v1/noticias/155
```json
{
  "noticia":{
    "id":"155",
    "titulo":"Demo de noticia 2",
    "contenido":"Contenido de noticia 2",
    "createdAt": "2015-04-15T10:58:35.040Z",
    "updatedAt": "2015-04-15T10:58:35.040Z",
    "category": [ "news", "actualidad", "ocio" ],
    "keywords": [ "contenido", "noticia", "demo" ],
    "slug":"demo-de-noticia-2"
  }
}
```
Respuesta ok
```json
{
  "noticia":{
    "id":"155",
    "titulo":"Demo de noticia 2",
    "contenido":"Contenido de noticia 2",
    "createdAt": "2015-04-15T10:58:35.040Z",
    "updatedAt": "2015-04-15T10:58:35.040Z",
    "category": [ "news", "actualidad", "ocio" ],
    "keywords": [ "contenido", "noticia", "demo" ],
    "slug":"demo-de-noticia-2"
  },
  "stat":"ok"
}
```

#### Eliminar una transacción
Solicitud [DELETE] /api/v1/noticias/155
Respuesta (cod. 204)



## Cosas pendientes
- Validar fallos
- Paginar resultados
- Implementar autorizacion
- Filtrar por categorias
- Buscar por slug
