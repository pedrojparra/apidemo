'use strict';

/**
 * Dependencies
 */
var express = require('express');
var app = express.Router();
var lowerCase = require('lower-case');
var slug = require('slug');

/**
 * Locals
 */
var Noticia = require('./model');
var version = '/api/v1';
var rutaBase = '/noticias/';

/**
 * Verbs
 */

app.get(version+rutaBase+'reset', function(req, res){
  Noticia.remove(function() {
    res
      .status(204)
      .send();
  });
});

app.get(version+rutaBase, function(req, res){
  Noticia.find({}).exec()
    .then(function(noticias) {
      var noticiasFixed = noticias.map(function(noticia) {
        return noticia.toJSON();
      });
      res
        .status(200)
        .set('Content-Type','application/json')
        .json({
          noticias: noticiasFixed,
          stat: 'ok'
        });
     }, function(err) {
       console.log('err', err);
    });
});


app.route(version+rutaBase+':id?')

  // Para todas las peticiones a esta ruta
  .all(function(req, res, next) {
    // console.log('All Info: ', req.method, req.path, req.body);
    res.set('Content-Type','application/json');
    next();
  })

  // POST
  .post(function(req, res) {
    var fecha = new Date();
    // manipulate request
    var noticiaNueva = req.body.noticia;
    noticiaNueva.createdAt = fecha;
    noticiaNueva.updatedAt = fecha;
    noticiaNueva.slug = lowerCase(slug(noticiaNueva.titulo));

    // Guardamos
    Noticia.create(noticiaNueva, function(err, noticia){
      if (err) {
        res
          .status(422)
          .json({
            stat: 'fail',
            info: err
          });
      }
      else{
        res
          .status(201)
          .json({
            noticia: noticia.toJSON(),
            stat: 'ok'
          });
      }
    });

  })


  // PUT /noticias
  .put(function(req, res, next) {

    var fecha = new Date();
    var id = req.params.id;
    var noticiaActualizada = req.body.noticia;
    noticiaActualizada.updatedAt = fecha;
    if (!id) { return next(); }
    Noticia.update({_id:id}, noticiaActualizada, function(err, results) {
      if(err){
        res
          .status(422)
          .json({
            stat: 'fail',
            info: err
          });
      }
      if (results.ok) {
        return res
          .status(200)
          .json({
            noticias: noticiaActualizada,
            stat: 'ok'
          });
      }
    });

  })

  // GET /noticias
  .get(function(req, res, next) {

    var id = req.params.id;
    if (!id) {
      console.log('no param');
      return next();
    }

    Noticia.findById(id, function(err, noticia) {

      if (!noticia) {
        /* return res.status(422).send({});  */
        return res
          .status(422)
          .json({
            stat: 'fail',
            info: err
          });
      }else{
        return res
          .status(200)
          .json({
            noticias: noticia,
            stat: 'ok'
          });
      }

    });

  })

  // DELETE
  .delete( function(req, res, next) {
    var id = req.params.id;
    if (!id) {
      console.log('no param');
      return next();
    }
    Noticia.remove({_id:id}, function(err) {
      if(err){
        return res
          .status(422)
          .json({
            stat: 'fail',
            info: err
          });
      }else{
        return res
          .status(204)
          .send();
      }

    });
  });


module.exports = app;
