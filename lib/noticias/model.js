'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NoticiaSchema = new Schema({
  titulo: { type: String, required: true },
  contenido: { type: String },
  urlImg: { type: String, required: true },
  keywords: { type: [String], index: true },
  category: { type: [String], index: true },
  createdAt: { type: Date },
  updatedAt: { type: Date },
  slug: { type: String, required: true }
});

// Function to map in get transacciones
NoticiaSchema.set('toJSON', {
  transform: function (doc, ret, options){
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  }
});

var model = mongoose.model('noticias', NoticiaSchema);

module.exports = model;
