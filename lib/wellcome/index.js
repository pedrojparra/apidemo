'use strict';

/**
 * Dependencies
 */
var app = require('express')();

/**
 * Verbs
 */

app.get('/', function(req, res){

  res
    .status(201)
    .set('Content-Type','application/json')
    .json({'Api':'Demo de api'});

});

module.exports = app;
