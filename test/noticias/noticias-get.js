'use strict';

/**
 * Dependencies
 */

 var request = require('supertest-as-promised');
 var mongoose = require('mongoose');
 var _ = require('lodash');

 var api = require('../../server.js');
 var environment = require('../../config/environment');
 var config = environment(process.env.NODE_ENV);
 var fakeData = require('./noticiasFakeData');

 var host = process.env.OPENSHIFT_NODEJS_IP || api;
 var mongolabURI = config.mongolabURI;
 var rutaBase = '/api/v1/noticias/';

describe('Metodos Get para coleccion de Noticias v1 [/api/v1/noticicas]', function(){

  // Conectamos con la base de datos antes de empezar los test
  before(function(done) {
    mongoose.connect(mongolabURI, done);
  });

  after(function(done) {
    mongoose.disconnect(done);
    mongoose.models = {};
  });

  // Empezamos los test

  describe('RESET - Resetear tabla de noticias', function(){
    it('Debera resetear ls tabla de noticias', function(done){
      request(host)
      .get(rutaBase+'reset')
      .set('Accept', 'application/json')
      .send()
      .expect(204)
      .then(function comprobamos(){
        done();
      }, done);
    });
  });

  describe('GET Empty /api/v1/noticias/', function() {
    it('Deberia obtener un array vacio al pedir todas las noticia existentes', function(done){

      return request(host)
      .get(rutaBase)
      .set('Accept', 'application/json')
      .expect(200)
      .expect('Content-Type', /application\/json/)
      .then(function comprobamos(res, err){

        var body = res.body;
        expect(body).to.have.property('noticias');
        expect(body).to.have.property('stat','ok');
        expect(body.noticias).to.be.an('array').to.have.length(0);
        done(err);
      },done);

    });
  });


  describe('GET FAIL /api/v1/noticias/:id', function() {
    it('Deberia falla al obtener una noticia no existente', function(done) {
      var idNoticia = 'xxxxxxxx';
      request(host)
        .get(rutaBase + idNoticia)
        .set('Accept', 'application/json')
        .send()
        .expect(422)
        .expect('Content-Type', /application\/json/)
        .then(function comprobamos(res, err){

          var body = res.body;
          expect(body).to.have.property('stat','fail');
          expect(body).to.have.property('info');
          done(err);

        },done);
    });
  });

  describe('GET /api/v1/noticias/:id', function() {
    it('Deberia obtener una noticia existente', function(done) {

      var id;
      var fecha;
      var data = fakeData.uno;

      request(host)
      .post(rutaBase)
      .set('Accept', 'application/json')
      .send(data)
      .expect(201)
      .expect('Content-Type', /application\/json/)

      .then(function getNoticia(res) {
        id = res.body.noticia.id;
        fecha = res.body.noticia.createdAt;
        return request(host)
        .get(rutaBase + id)
        .set('Accept', 'application/json')
        .send()
        .expect(200)
        .expect('Content-Type', /application\/json/);
      }, done)

      .then(function comprobamos(res, err){
        var body = res.body;
        expect(body).to.have.property('noticias');
        expect(body).to.have.property('stat','ok');
        // Validamos los campos de la respuesta
        var noticia = body.noticias;
        expect(noticia).to.have.property('titulo', 'Demo de noticia');
        expect(noticia).to.have.property('contenido', 'Contenido de noticia');
        expect(noticia).to.have.property('urlImg', 'http://res.cloudinary.com/demo/image/upload/v1371281596/sample.jpg');
        expect(noticia).to.have.property('keywords').to.have.length(3).to.eql(['contenido','noticia','demo']);
        expect(noticia).to.have.property('category').to.have.length(3).to.eql(['news','actualidad','ocio']);
        expect(noticia).to.have.property('slug', 'demo-de-noticia');
        expect(noticia).to.have.property('createdAt',fecha);
        expect(noticia).to.have.property('updatedAt',fecha);
        expect(noticia).to.have.property('id', id);

        done(err);
      }, done);

    });
  });



  describe('GET /api/v1/noticias/', function() {
    it('Deberia obtener todas las noticias existente', function(done) {

      var id1, id2, fechaCreacion1, fechaCreacion2, fechaActualizacion1, fechaActualizacion2;

      var data1 = fakeData.uno;
      var data2 = fakeData.dos;

      // Creamos la noticia
      request(host)
      .post(rutaBase)
      .set('Accept', 'application/json')
      .send(data1)
      .expect(201)
      .expect('Content-Type', /application\/json/)

      .then(function crearOtraNoticia(res){
        id1 = res.body.noticia.id;
        fechaCreacion1 = res.body.noticia.createdAt;
        fechaActualizacion1 = res.body.noticia.updatedAt;
        return request(host)
          .post(rutaBase)
          .set('Accept', 'application/json')
          .send(data2)
          .expect(201)
          .expect('Content-Type', /application\/json/);
      }, done )

      .then(function getNotas(res) {

        id2 = res.body.noticia.id;
        fechaCreacion2 = res.body.noticia.createdAt;
        fechaActualizacion2 = res.body.noticia.updatedAt;
        return request(host)
          .get(rutaBase)
          .set('Accept', 'application/json')
          .expect(200)
          .expect('Content-Type', /application\/json/);

      }, done)

      .then(function comprobamos(res, err){

        var body = res.body;

        expect(body).to.have.property('noticias');
        expect(body).to.have.property('stat','ok');

        expect(body.noticias).to.be.an('array').and.to.have.length.above(2);

        var noticias = body.noticias;
        var noticia1 = _.find(noticias, {id:id1});
        var noticia2 = _.find(noticias, {id:id2});

        // Propiedades noticia1
        expect(noticia1).to.have.property('titulo', 'Demo de noticia');
        expect(noticia1).to.have.property('contenido', 'Contenido de noticia');
        expect(noticia1).to.have.property('urlImg', 'http://res.cloudinary.com/demo/image/upload/v1371281596/sample.jpg');
        expect(noticia1).to.have.property('keywords').to.have.length(3).to.eql(['contenido','noticia','demo']);
        expect(noticia1).to.have.property('category').to.have.length(3).to.eql(['news','actualidad','ocio']);
        expect(noticia1).to.have.property('slug', 'demo-de-noticia');
        expect(noticia1).to.have.property('createdAt',fechaCreacion1);
        expect(noticia1).to.have.property('updatedAt',fechaActualizacion1);
        expect(noticia1).to.have.property('id', id1);

        // Propiedades noticia2
        expect(noticia2).to.have.property('titulo', 'Demo de noticia 2');
        expect(noticia2).to.have.property('contenido', 'Contenido de noticia 2');
        expect(noticia2).to.have.property('urlImg', 'http://res.cloudinary.com/demo/image/upload/v1371281596/sample.jpg');
        expect(noticia2).to.have.property('keywords').to.have.length(3).to.eql(['contenido2','noticia2','demo2']);
        expect(noticia2).to.have.property('category').to.have.length(1).to.eql(['cultura']);
        expect(noticia2).to.have.property('slug', 'demo-de-noticia-2');
        expect(noticia2).to.have.property('createdAt',fechaCreacion2);
        expect(noticia2).to.have.property('updatedAt',fechaActualizacion2);
        expect(noticia2).to.have.property('id', id2);

        done(err);

      }, done);

    });
  });


});
