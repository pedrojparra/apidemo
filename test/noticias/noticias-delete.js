'use strict';

/**
 * Dependencies
 */

var request = require('supertest-as-promised');
var mongoose = require('mongoose');
var _ = require('lodash');

var api = require('../../server.js');
var environment = require('../../config/environment');
var config = environment(process.env.NODE_ENV);
var fakeData = require('./noticiasFakeData');

var host = process.env.OPENSHIFT_NODEJS_IP || api;
var mongolabURI = config.mongolabURI;
var rutaBase = '/api/v1/noticias/';

describe('Metodos Delete para coleccion de Noticias v1 [/api/v1/noticicas]', function(){

  // Conectamos con la base de datos antes de empezar los test
  before(function(done) {
    mongoose.connect(mongolabURI, done);
  });

  after(function(done) {
    mongoose.disconnect(done);
    mongoose.models = {};
  });

  // Empezamos los test

  describe('RESET - Resetear tabla de noticias', function(){
    it('Debera resetear ls tabla de noticias', function(done){
      request(host)
      .get(rutaBase+'reset')
      .set('Accept', 'application/json')
      .send()
      .expect(204)
      .then(function comprobamos(){
        done();
      }, done);
    });
  });

  describe('DELETE FAIL /api/v1/noticias/:id', function(){
    it('Debera fallar al eliminar una noticia no existente', function(done){

      var idNoticia = 'xxxxxxxx';
      request(host)
        .delete(rutaBase + idNoticia)
        .set('Accept', 'application/json')
        .send()
        .expect(422)
        .expect('Content-Type', /application\/json/)
        .then(function comprobamos(res, err){

          var body = res.body;
          expect(body).to.have.property('stat','fail');
          expect(body).to.have.property('info');
          done(err);

        },done);

    });
  });

  describe('DELETE /api/v1/noticias/:id', function(){
    it('Debera eliminar una noticia nueva', function(done){

      var id;
      var data = fakeData.uno;

      // Creamos la noticia
      request(host)
      .post(rutaBase)
      .set('Accept', 'application/json')
      .send(data)
      .expect(201)
      .expect('Content-Type', /application\/json/)

      .then(function deleteNoticia(res){
        id = res.body.noticia.id;
        return request(host)
          .delete(rutaBase+id)
          .set('Accept', 'application/json')
          .send()
          .expect(204);
      }, done)

      // Comprobamos
      .then(function comprobamos(){
        return request(host).get(rutaBase + id)
          .set('Accept', 'application/json')
          .send()
          .expect(422);
      }, done)

      // Confirmamos que no existe
      .then(function confirmation(res) {
        var body = res.body;
        expect(body).to.have.property('stat','fail');
        expect(body).to.have.property('info');
        done();
      }, done);

    });
  });

});
