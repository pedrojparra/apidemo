'use strict';

var fakeData = {

  'uno':{
    'noticia':{
      'titulo':'Demo de noticia',
      'contenido':'Contenido de noticia',
      'urlImg':'http://res.cloudinary.com/demo/image/upload/v1371281596/sample.jpg',
      'keywords': ['contenido','noticia','demo'],
      'category': ['news','actualidad','ocio']
    }
  },

  'dos':{
    'noticia':{
      'titulo':'Demo de noticia 2',
      'contenido':'Contenido de noticia 2',
      'urlImg':'http://res.cloudinary.com/demo/image/upload/v1371281596/sample.jpg',
      'keywords': ['contenido2','noticia2','demo2'],
      'category': ['cultura']
    }
  },

  'fail':{
    'noticia':{
      'titulo':'',
      'contenido':'Contenido de noticia',
      'urlImg':'http://res.cloudinary.com/demo/image/upload/v1371281596/sample.jpg',
      'keywords': ['contenido','noticia','demo'],
      'category': ['news','actualidad','ocio']
    }
  }

};

module.exports = fakeData;
