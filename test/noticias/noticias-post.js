'use strict';

/**
 * Dependencies
 */

 var request = require('supertest-as-promised');
 var mongoose = require('mongoose');
 var _ = require('lodash');

 var api = require('../../server.js');
 var environment = require('../../config/environment');
 var config = environment(process.env.NODE_ENV);
 var fakeData = require('./noticiasFakeData');

 var host = process.env.OPENSHIFT_NODEJS_IP || api;
 var mongolabURI = config.mongolabURI;
 var rutaBase = '/api/v1/noticias/';

describe('Metodos Post para coleccion de Noticias v1 [/api/v1/noticicas]', function(){

  // Conectamos con la base de datos antes de empezar los test
  before(function(done) {
    mongoose.connect(mongolabURI, done);
  });

  after(function(done) {
    mongoose.disconnect(done);
    mongoose.models = {};
  });

  // Empezamos los test

  describe('RESET - Resetear tabla de noticias', function(){
    it('Debera resetear ls tabla de noticias', function(done){
      request(host)
      .get(rutaBase+'reset')
      .set('Accept', 'application/json')
      .send()
      .expect(204)
      .then(function comprobamos(){
        done();
      }, done);
    });
  });

  describe('POST FAIL /api/v1/noticias', function(){
    it('Debera fallar al crear una noticia sin campos requeridos', function(done){

      var data = fakeData.fail;
      request(host)
        .post(rutaBase)
        .set('Accept', 'application/json')
        .send(data)
        .expect(422)
        .expect('Content-Type', /application\/json/)
        .then(function comprobamos(res, err){

          var body = res.body;
          expect(body).to.have.property('stat','fail');
          expect(body).to.have.property('info');
          done(err);

        },done);

    });
  });

  describe('POST OK /api/v1/noticias', function(){
    it('Debera crear una noticia nueva', function(done){

      var data = fakeData.uno;

      request(host)
      .post(rutaBase)
      .set('Accept', 'application/json')
      .send(data)
      .expect(201)
      .expect('Content-Type', /application\/json/)
      .then(function comprobamos(res, err){

        var body = res.body;
        expect(body).to.have.property('noticia');
        expect(body).to.have.property('stat','ok');

        // Validamos los campos de la respuesta
        var noticia = body.noticia;
        expect(noticia).to.have.property('titulo', 'Demo de noticia');
        expect(noticia).to.have.property('contenido', 'Contenido de noticia');
        expect(noticia).to.have.property('urlImg', 'http://res.cloudinary.com/demo/image/upload/v1371281596/sample.jpg');
        expect(noticia).to.have.property('keywords').to.have.length(3).to.eql(['contenido','noticia','demo']);
        expect(noticia).to.have.property('category').to.have.length(3).to.eql(['news','actualidad','ocio']);
        expect(noticia).to.have.property('slug', 'demo-de-noticia');
        expect(noticia).to.have.property('createdAt');
        expect(noticia).to.have.property('updatedAt');
        expect(noticia).to.have.property('id');

        done(err);

      }, done);

    });
  });

});
