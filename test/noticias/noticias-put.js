'use strict';

/**
 * Dependencies
 */

 var request = require('supertest-as-promised');
 var mongoose = require('mongoose');
 var _ = require('lodash');

 var api = require('../../server.js');
 var environment = require('../../config/environment');
 var config = environment(process.env.NODE_ENV);
 var fakeData = require('./noticiasFakeData');

 var host = process.env.OPENSHIFT_NODEJS_IP || api;
 var mongolabURI = config.mongolabURI;
 var rutaBase = '/api/v1/noticias/';

describe('Metodos PUT para coleccion de Noticias v1 [/api/v1/noticicas]', function(){

  // Conectamos con la base de datos antes de empezar los test
  before(function(done) {
    mongoose.connect(mongolabURI, done);
  });

  after(function(done) {
    mongoose.disconnect(done);
    mongoose.models = {};
  });

  // Empezamos los test

  describe('RESET - Resetear tabla de noticias', function(){
    it('Debera resetear ls tabla de noticias', function(done){
      request(host)
      .get(rutaBase+'reset')
      .set('Accept', 'application/json')
      .send()
      .expect(204)
      .then(function comprobamos(){
        done();
      }, done);
    });
  });

  describe('PUT FAIL /api/v1/noticias/:id', function() {
    it('Deberia fallar al actualizar una noticia no existente', function(done){

      var id = 'xxx';
      var update = {
        'noticia':{
          'titulo':'Demo de noticia',
          'contenido':'Contenido de noticia',
          'urlImg':'http://res.cloudinary.com/demo/image/upload/v1371281596/sample.jpg',
          'keywords': ['contenido','noticia','demo'],
          'category': ['news','actualidad','ocio']
        }
      };
      return request(host)
      .put(rutaBase + id)
      .set('Accept', 'application/json')
      .send(update)
      .expect(422)
      .expect('Content-Type', /application\/json/)
      .then(function comprobamos(res, err){
        var body = res.body;
        expect(body).to.have.property('stat','fail');
        expect(body).to.have.property('info');
        done(err);
      }, done);
    });
  });



  describe('PUT OK /api/v1/noticias/:id', function() {
    it('Deberia actualizar una noticia existente', function(done) {

      var id;
      var fechaCreacion;
      var fechaActualizacion;
      var data = fakeData.uno;

      request(host)
      .post(rutaBase)
      .set('Accept', 'application/json')
      .send(data)
      .expect(201)
      .expect('Content-Type', /application\/json/)

      .then(function updateNoticia(res) {

        id = res.body.noticia.id;
        fechaCreacion = res.body.noticia.createdAt;
        fechaActualizacion = res.body.noticia.updatedAt;
        var update = {
          'noticia':{
            'id': id,
            'titulo':'Demo de noticia 2',
            'contenido':'Contenido de noticia 2',
            'urlImg':'http://res.cloudinary.com/demo/image/upload/v1371281597/sample.jpg',
            'keywords': ['contenido2','noticia2'],
            'category': ['cultura'],
            'slug':'demo-de-noticia-2',
            'createdAt': fechaCreacion,
            'updatedAt': fechaActualizacion
          }
        };

        return request(host)
        .put(rutaBase + id)
        .set('Accept', 'application/json')
        .send(update)
        .expect(200)
        .expect('Content-Type', /application\/json/);

      }, done)

      .then(function comprobamos(res, err){
        var body = res.body;
        expect(body).to.have.property('noticias');
        expect(body).to.have.property('stat','ok');
        // Validamos los campos de la respuesta
        var noticia = body.noticias;
        expect(noticia).to.have.property('titulo', 'Demo de noticia 2');
        expect(noticia).to.have.property('contenido', 'Contenido de noticia 2');
        expect(noticia).to.have.property('urlImg', 'http://res.cloudinary.com/demo/image/upload/v1371281597/sample.jpg');
        expect(noticia).to.have.property('keywords').to.have.length(2).to.eql(['contenido2','noticia2']);
        expect(noticia).to.have.property('category').to.have.length(1).to.eql(['cultura']);
        expect(noticia).to.have.property('slug', 'demo-de-noticia-2');
        expect(noticia).to.have.property('createdAt', fechaCreacion);
        expect(noticia).to.have.property('updatedAt').to.not.equal(fechaActualizacion);
        expect(noticia).to.have.property('id', id);

        done(err);
      }, done);

    });
  });

});
